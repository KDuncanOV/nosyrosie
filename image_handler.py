#!/usr/bin/env python
import datetime
import time
import copy
import cv2
import logging
import pantilthat as pt
from threading import Thread
from Queue import Queue
from camera_capture import NosyVideoStream
from image import NosyImage

logging.basicConfig(level=logging.DEBUG, format='[%(threadName)-10s] %(message)s')


#=======================================================================
# @brief Class using the NosyVideoStream to grab frames and send them
#		 for processing
#=======================================================================
class ImageHandler(Thread):
	capture = False
	frequency = 1
	processingQueue = []

	#
	# @brief Constructor
	#
	def __init__(self, videoStream, pQueue):
		Thread.__init__(self, group=None, target=None, name="ImageHandler", verbose=False)
		self.stream = videoStream	
		self.processingQueue = pQueue	

	#
	# @brief Destructor
	#
	def __del__(self):
		self.stop()

	#
	# @brief Execute the image capturing thread
	#
	def run(self):
		logging.info("Capturing Images...")
				
		try:
			self.capture = True
						
			while self.capture == True:
				time.sleep(1 / self.frequency)
				frame = copy.deepcopy(self.stream.read())
				timestamp = datetime.datetime.now()	
				nosyImage = NosyImage(frame, timestamp, pt.get_pan())				
				self.processingQueue.put(nosyImage, timeout=10)					
				
			logging.info("Completing Image Capture")
			return

		except KeyboardInterrupt as k:
			logging.warn("Exiting due to keyboard interrupt")
			self.stop()
			return
				
		except Exception as ex:
			logging.error("An unexpected exception has occurred: " + str(ex))
			self.stop()
			return


	#
	# @brief Stop executing motion
	#
	def stop(self):
		self.capture = False        
		self.stream.stop()		
