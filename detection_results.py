#!/usr/bin/env python


#=======================================================================
# @brief Class representing the results of object detection
#=======================================================================
class DetectionResults:
	timestamp = 0
	degree = 0
	numDetections = 0
	
	def __init__(self, ts, deg, detections):
		self.timestamp = ts
		self.degree = deg
		self.numDetections = detections
		
	def getTimestamp(self):
		return self.timestamp
		
	def getDegree(self):
		return self.degree
		
	def getDetectionCount(self):
		return self.numDetections
