#!/usr/bin/env python

import time
import copy
import logging
from picamera import PiCamera
from picamera.array import PiRGBArray
from threading import Thread, Lock
import cv2

	
#=======================================================================
# @brief Video stream for the PiCamera
#=======================================================================
class NosyVideoStream:
	frameLock = Lock()
		
	def __init__(self, resolution=(320, 240), framerate=33):
		# initialize the camera and stream
		try:
			self.camera = PiCamera()
			self.camera.rotation = 180
			self.camera.resolution = resolution
			self.camera.framerate = framerate
			self.rawCapture = PiRGBArray(self.camera, size=resolution)
			self.stream = self.camera.capture_continuous(self.rawCapture, 
														 format="bgr", 
														 use_video_port=True)
		except Exception as ex:
			logging.error("Unable to initialize the camera stream. Ensure that the camera is connected and not in use")
			raise			

		# initialize the frame and the variable used to indicate
		# if the thread should be stopped
		self.frame = None
		self.stopped = False
		
	def __del__(self):
		self.camera.close()


	def start(self):
		# start the thread to read frames from the video stream
		t = Thread(target=self.update, args=())
		t.daemon = False
		t.start()
		return self


	def update(self):
		# keep looping infinitely until the thread is stopped
		for f in self.stream:
			# grab the frame from the stream and clear the stream in
			# preparation for the next frame
			self.frameLock.acquire()
			self.frame = f.array
			self.frameLock.release()
			
			self.rawCapture.truncate(0)			

			# if the thread indicator variable is set, stop the thread
			# and resource camera resources
			if self.stopped:
				self.stream.close()
				self.rawCapture.close()
				self.camera.close()
				return

	def read(self):
		# return the frame most recently read
		currFrame = []
		self.frameLock.acquire()
		#currFrame = copy.deepcopy(self.frame)
		currFrame = self.frame
		self.frameLock.release()
		
		return currFrame

	def stop(self):
		# indicate that the thread should be stopped
		if self.stopped is not True:
			self.stopped = True	



#=======================================================================
# @brief Camera capture class to get frames
#=======================================================================
class CameraCapture:
	def __init__(self, resolution=(320, 240), framerate=33):
		# Initialize the picamera stream and allow the camera
		# sensor to warmup
		self.stream = NosyVideoStream(resolution=resolution, 
									  framerate=framerate)
		
	def __del__(self):
		self.stream.stop()
		self.stream.camera.close()
		self.stop()
		
	def start(self):
		# Start the threaded video stream
		return self.stream.start()

	def update(self):
		# Grab the next frame from the stream
		self.stream.update()

	def getFrame(self):
		# Return the current frame
		return self.stream.read()

	def stop(self):
		# Stop the thread and release any resources
		self.stream.stop()
		
