#!/usr/bin/env python

import math
import time

import pantilthat

pantilthat.pan(0)
pantilthat.tilt(0)
time.sleep(2)

counter = -1.0

try:
	while True:
		# Get the time in seconds
		t = time.time()
		
		if counter > 1.0:
			counter = -1.0
		
		# Generate an angle using a sine wave (-1 to 1) multiplied by 90 (-90 to 90)
		#a = math.sin(t * 2) * 60
		a = math.sin(counter) * 60
		
		# Cast a to int for v0.0.2
		a = int(a)

		pantilthat.pan(a)
		#pantilthat.tilt(a)

		# Two decimal places is quite enough!
		print(round(a, 2))

		# Sleep for a bit so we're not hammering the HAT with updates
		time.sleep(0.005)
		counter += 0.01
		
except KeyboardInterrupt as k:
	print
	print ">>> Warning: Closing due to keyboard interrupt"
	print
		
except Exception as ex:
	print
	print ">>> Error: An unexpected exception has occurred"
	print
		
