#!/usr/bin/env python
import time
import logging
import copy
import numpy as np
from pan_tilt_manager import PanTiltManager
from threading import Thread

logging.basicConfig(level=logging.DEBUG, format='[%(threadName)-10s] %(message)s')


#=======================================================================
# @brief Class using the pan-tilt-manager to operate the camera
#=======================================================================
class ZoneMapHandler(Thread):
	process = False
	resultsQueue = []		
		
	#
	# #brief Constructor
	#
	def __init__(self, rQueue, zMap):
		Thread.__init__(self, group=None, target=None, name="ZoneMapHandler", verbose=False)
		self.resultsQueue = rQueue
		self.zoneMap = zMap
		return
		
	
	#
	# @brief Destructor
	#
	def __del__(self):
		self.stop()


	#
	# @brief Execute the map update
	#
	def run(self):
		logging.info("Starting...")
		try:
			self.process = True		
									
			while self.process == True:
				results = copy.deepcopy(self.resultsQueue.get())	
				degree = results.getDegree()
				detectionCnt = results.getDetectionCount()
				degreeIdx = self.zoneMap.degreeToIndex(degree)
				
				if degreeIdx >= 0:
					self.zoneMap.incrementMapCount(degreeIdx, detectionCnt)							
								
			logging.info("Completing Zone Map Handling")
			return

		except KeyboardInterrupt as k:
			logging.warn("Exiting due to keyboard interrupt")
			self.stop()
			return
				
		except Exception as ex:
			logging.error("An unexpected exception has occurred: " + str(ex))
			self.stop()
			return


	#
	# @brief Stop executing motion
	#
	def stop(self):
		self.process = False		
