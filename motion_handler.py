#!/usr/bin/env python
import time
import logging
import math
import numpy as np
from scipy.ndimage.filters import gaussian_filter
from pan_tilt_manager import PanTiltManager
from threading import Thread
from zone_map import ZoneMap

logging.basicConfig(level=logging.DEBUG, format='[%(threadName)-10s] %(message)s')


#=======================================================================
# @brief Fake Enum representing the motion direction
#=======================================================================
class MotionDirection:
	Forward = 1
	Backward = -1


#=======================================================================
# @brief Class using the pan-tilt-manager to operate the camera
#=======================================================================
class MotionHandler(Thread):
	CYCLE_TIME = 4500
	performMotion = False
	cycles = 0
	speed = 50
	ptManager = (True)
	degrees = np.arange(-60, 60, 0.5)
	degreeWeights = np.ones(len(degrees))	
	
	#
	# #brief Constructor
	#
	def __init__(self, panTiltManager, zMap):
		Thread.__init__(self, group=None, target=None, name="MotionHandler", verbose=False)
		self.ptManager = panTiltManager
		self.zoneMap = zMap
		self.degreeWeights = self.degreeWeights / float(len(self.degreeWeights))		
		return


	#
	# @brief Execute the pan-tilt motion handler thread
	#
	def run(self):
		logging.info("Starting...")
		try:
			degreeIdx = 0
			totalValues = len(self.degrees)
			motionDirection = MotionDirection.Forward
			
			self.performMotion = True		
			self.ptManager.panSlowly(self.degrees[degreeIdx])			
						
			while self.performMotion == True:
				degreeIdx += int(motionDirection)					
				if degreeIdx < 0:
					degreeIdx = 0
					
				if degreeIdx >= len(self.degrees):
					degreeIdx = len(self.degrees) - 1;
										
				degree = self.degrees[degreeIdx]
								
				if degreeIdx == 0 or degreeIdx == (len(self.degrees) - 1):
					self.cycles += 1
					totalValues = self.zoneMap.getMapTotalCount()
					
					for i in xrange(len(self.zoneMap.theCounts)):
						self.degreeWeights[i] = self.zoneMap.getMapCount(i) / totalValues
						
					#self.degreeWeights = gaussian_filter(self.degreeWeights, sigma=3)
						
					if motionDirection == MotionDirection.Forward:
						motionDirection = MotionDirection.Backward
					else:
						motionDirection = MotionDirection.Forward
					
				degree = int(degree)
				ms = float(self.degreeWeights[degreeIdx] * self.CYCLE_TIME)				
				self.ptManager.panToMillis(degree, ms)
				
								
			logging.info("Completing Motion Handling")
			return

		except KeyboardInterrupt as k:
			logging.warn("Exiting due to keyboard interrupt")
			self.stop()
			self.ptManager.reset()
			return
				
		except Exception as ex:
			logging.error("An unexpected exception has occurred during Motion Handling: " + str(ex))
			self.stop()
			self.ptManager.reset()
			return


	#
	# @brief Stop executing motion
	#
	def stop(self):
		self.performMotion = False		
		logging.warn("Stopping the Motion Handler")
		logging.info("**** Degree Weights: ****\n" + str(self.degreeWeights) + "\n")
