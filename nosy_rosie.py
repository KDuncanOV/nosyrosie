#!/usr/bin/env python
import sys
import logging
import time
import cv2
from Queue import Queue
from camera_capture import NosyVideoStream
from pan_tilt_manager import PanTiltManager
from motion_handler import MotionHandler
from display_handler import DisplayHandler
from image_handler import ImageHandler
from image_processor import ImageProcessor
from zone_map import ZoneMap
from zone_map_handler import ZoneMapHandler


logging.basicConfig(level=logging.DEBUG,
					format='[%(threadName)-10s] %(message)s',
				    )

#-----------------------------------------------------------------------
# Variable initialization
#-----------------------------------------------------------------------
caffeProto = '/home/pi/Code/NosyRosie/cfg/MobileNetSSD_deploy.prototxt.txt'
caffeModel = '/home/pi/Code/NosyRosie/cfg/MobileNetSSD_deploy.caffemodel'
processingQueue = Queue(100)
resultsQueue = Queue(200)
zoneMap = ZoneMap()
ptManager = PanTiltManager()
videoStream = NosyVideoStream(resolution=(640, 480), framerate=60).start()
displayHandler = DisplayHandler(videoStream)
motionHandler = MotionHandler(ptManager, zoneMap)
zoneMapHandler = ZoneMapHandler(resultsQueue, zoneMap)
imageHandler = ImageHandler(videoStream, processingQueue)
imageProcessor = ImageProcessor(processingQueue, resultsQueue, caffeProto, caffeModel)


#-----------------------------------------------------------------------
		
		
#
# Nosy Rosie Driver
# 	o operates the camera
#	o performs object detection
#	o focuses the camera view based on information found
#
def main():
	try:
		logging.info("Starting the Nosy Rosie Application")		
		time.sleep(1)
		
		displayHandler.start()	
		motionHandler.start()
		imageHandler.start()
		imageProcessor.start()
		zoneMapHandler.start()
		
		# Wait for the display to end
		displayHandler.join()
		
		# Stop all other threads
		displayHandler.stop()
		motionHandler.stop()			
		imageHandler.stop()
		imageProcessor.stop()
		zoneMapHandler.stop()
					
		logging.info("Stopping the Nosy Rosie Application")
		return 0		
		
	except Exception as ex:
		motionHandler.stop()
		motionHandler.join()
		
		displayHandler.stop()
		displayHandler.join()
		
		imageHandler.stop()
		imageHandler.join()
		
		imageProcessor.stop()
		imageProcessor.join()
		
		zoneMapHandler.stop()
		zoneMapHandler.join()
		
		logging.error("Exiting the application due to unhandled exception: " + str(ex))
		sys.exit(1)
		
		
if __name__ == '__main__':
	main()
