#!/usr/bin/env python
import numpy as np
import time
import cv2
import logging
from threading import Thread
from Queue import Queue
from image import NosyImage
from object_detector import ObjectDetector
from detection_results import DetectionResults

logging.basicConfig(level=logging.DEBUG, format='[%(threadName)-10s] %(message)s')


#=======================================================================
# @brief Class that performs image analysis
#=======================================================================
class ImageProcessor(Thread):
	process = False
	inputQueue = []
	outputQueue = []

	#
	# @brief Constructor
	#
	def __init__(self, iQueue, oQueue, prototextFile, modelFile):
		Thread.__init__(self, group=None, target=None, name="ImageProcessor", verbose=False)
		self.inputQueue = iQueue
		self.outputQueue = oQueue	
		self.objDetector = ObjectDetector(prototextFile, modelFile)	

	#
	# @brief Destructor
	#
	def __del__(self):
		self.stop()

	#
	# @brief Execute the image processing thread
	#
	def run(self):
		logging.info("Processing Images...")
				
		try:
			self.process = True
						
			while self.process == True:
				nosyImage = self.inputQueue.get(timeout=10)
				numDetectedItems = self.objDetector.detect(nosyImage.getImage())
				detectionResults = DetectionResults(nosyImage.timestamp, nosyImage.degree, numDetectedItems)
				self.outputQueue.put(detectionResults, timeout=10)
								
			logging.info("Completing Image Processing")
			return

		except KeyboardInterrupt as k:
			logging.warn("Exiting due to keyboard interrupt")
			self.stop()
			return
				
		except Exception as ex:
			logging.error("An unexpected exception has occurred: " + str(ex))
			self.stop()
			return

	#
	# @brief Stop executing motion
	#
	def stop(self):
		self.process = False
			
