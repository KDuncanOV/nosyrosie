#!/usr/bin/env python
import datetime
import time
import cv2
import logging
from threading import Thread, Event
from camera_capture import NosyVideoStream

logging.basicConfig(level=logging.DEBUG, format='[%(threadName)-10s] %(message)s')


#=======================================================================
# @brief Class using the NosyVideoStream to grab and display frames
#=======================================================================
class DisplayHandler(Thread):
	display = False
	
	#
	# #brief Constructor
	#
	def __init__(self, videoStream):
		Thread.__init__(self, group=None, target=None, name="DisplayHandler", verbose=False)
		self.stream = videoStream
		self.stopEvent = Event()

	#
	# @brief Destructor
	#
	def __del__(self):
		self.stop()

	#
	# @brief Execute the pan-tilt motion handler thread
	#
	def run(self):
		logging.info("Displaying Frames...")
				
		try:
			self.display = True
			
			while self.display == True: # and ~self.stopped():
				frame = self.stream.read()
				#timestamp = datetime.datetime.now()
				#ts = timestamp.strftime("%A %d %B %Y %I:%M:%S%p")
				#cv2.putText(frame, ts, (10, frame.shape[0] - 10), 
				#			cv2.FONT_HERSHEY_SIMPLEX,
				#			0.50, (0, 140, 255), 2)

				cv2.imshow("Nosy Rosie (Live View)", frame)
				key = cv2.waitKey(1) & 0xFF

				if key == ord("q"):
					self.stop()													
					break
				
			logging.info("Completing display handling")
			return

		except KeyboardInterrupt as k:
			logging.warn("Exiting due to keyboard interrupt")
			self.stop()
			self.reset()
			return
				
		except Exception as ex:
			logging.error("An unexpected exception has occurred: " + str(ex))
			self.stop()
			self.reset()
			return

	#
	# @brief Stop executing motion
	#
	def stop(self):
		cv2.waitKey(1)
		cv2.destroyAllWindows()
		cv2.waitKey(1)
		self.stopEvent.set()
		self.display = False        
		self.stream.stop()
		time.sleep(1)
		
	#
	# @brief Checks if the thread is stopped
	#
	def stopped(self):
		return self.stopEvent.is_set()
