#!/usr/bin/env python

import cv2
import numpy as np
import logging

from image import NosyImage

logging.basicConfig(level=logging.DEBUG, format='[%(threadName)-10s] %(message)s')

CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
		   "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
		   "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
		   "sofa", "train", "tvmonitor"]

COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))


#=======================================================================
# @brief Class responsible for performing object detection using YOLO
#=======================================================================
class ObjectDetector:
	scale = 0.007843
	confidenceThreshold = 0.5
	drawResults = False	
	processed = 0
	
	#
	# @brief Constructor
	#
	def __init__(self, prototextFile, modelFile, draw=False):
		logging.info("Reading network configuration information...")
		self.net = cv2.dnn.readNetFromCaffe(prototextFile, modelFile)
		self.drawResults = draw
		logging.info("Reading network configuration information...DONE")
		
		
	#
	# @brief Detects objects in an image
	#
	def detect(self, origImage):
		imgW = origImage.shape[1]
		imgH = origImage.shape[0]
		image = cv2.resize(origImage, (300, 300))
		blob = cv2.dnn.blobFromImage(image, self.scale, (300, 300), 127.5)
		self.net.setInput(blob)
		detections = self.net.forward()
		validDetections = 0
		
		if detections is not None:
			self.processed += 1			
			logging.debug("Performing detection. Got {} result(s)!".format(detections.shape[2]))
			
			if detections.shape[2] < 5:
				for i in np.arange(0, detections.shape[2]):
					confidence = detections[0, 0, i, 2]
					
					if confidence >= self.confidenceThreshold:	
						idx = int(detections[0, 0, i, 1])
						
						#logging.info(">>> Found a {} <<<".format(CLASSES[idx]))
						
						if CLASSES[idx] is "person":
							validDetections += (800 + confidence)
													
						elif CLASSES[idx] is "dog":
							validDetections += (400 + confidence)
							
						elif CLASSES[idx] is "sofa" or CLASSES[idx] is "diningtable" or CLASSES[idx] is "chair":
							validDetections += (200 + confidence)
							
						else:
							validDetections += 100
							
						if self.drawResults:		
							dims = np.array([imgW, imgH, imgW, imgH])
							box = detections[0, 0, i, 3:7] * dims
							(startX, startY, endX, endY) = box.astype("int")

							label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
							cv2.rectangle(origImage, (startX, startY), (endX, endY), COLORS[idx], 2)
							y = startY - 15 if startY - 15 > 15 else startY + 15
							cv2.putText(origImage, label, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)

			if self.drawResults is True:
				imgName = "/home/pi/Desktop/Detections/ObjectDetectionOutput_%d.jpg" % (self.processed)
				cv2.imwrite(imgName, origImage)
		
		return validDetections
		
