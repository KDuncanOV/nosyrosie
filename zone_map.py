#!/usr/bin/env python
import numpy as np
import logging
from threading import Lock

logging.basicConfig(level=logging.DEBUG, format='[%(threadName)-10s] %(message)s')

#=======================================================================
# @brief Class representing the field of view zone map
#=======================================================================
class ZoneMap:
	theDegreeToIdxMap = {}
	theCounts = []
	theSum = 0
	mutex = Lock()
	
	def __init__(self):
		degrees = np.arange(-60, 60, 0.5)
		self.theCounts = np.ones(len(degrees))
		self.theSum = len(degrees)
		
		count = 0
		for i in degrees:
			self.theDegreeToIdxMap[i] = count
			count += 1
					
	def degreeToIndex(self, degree):
		try:
			if degree in self.theDegreeToIdxMap:
				val = self.theDegreeToIdxMap[degree]
			else:
				val = -1
			
		except Exception as ex:
			logging.warn("An exception was caught in the ZoneMap: " + str(ex))
			val = -1
			
		return val
		
	def incrementMapCount(self, idx, val):
		self.mutex.acquire()
		self.theCounts[idx] += val
		self.theSum += val
		self.mutex.release()
		
		
	def getMapCount(self, idx):
		self.mutex.acquire()
		val = self.theCounts[idx]
		self.mutex.release()
		
		return val
		
	def getMapTotalCount(self):
		self.mutex.acquire()
		val = self.theSum
		self.mutex.release()
		
		return val
		
		
	
