# NosyRosie

## Activity-Focused Pan Tilt Camera

The NosyRosie camera is an intelligent pan-tilt camera composed of a Raspberry Pi, a Raspberry Pi Cam 2 and the Pimoroni Pan Tilt Hat. Using object detection on captured frames, the camera biases its motion handler so that areas of high activity are focused on more.
This biasing is based on the number of objects detected (via a Mobile SSD object detector) and the type of object detected (coupled with a predetermined weighting e.g. if a person is detected, more weight is applied to the zone in which they were found). 
This occurs as a result of a zone map, which is a weighted probability distribution on the camera angle zones (-60 to 60 degrees - a zone is every 0.5 degrees for a total of 240 zones). The motion handler component of the camera uses this zone map for maneuvering.

Alarm currently doesn't possess an intelligent pan-tilt camera. Additionally, based on a cursory search, there are no commercially-viable intelligent pan-tilt cameras on the market. The closest camera is the Zmodo Pan & Tilt WiFi security camera (https://www.amazon.com/Zmodo-Security-Camera-2-way-Audio/dp/B00XPKXY3M). For this project, only the pan functionality was implemented.

## Team Member
Kester Duncan - Planning, Design, Implementation, Testing, and Presentation (EVERYTHING).

### Demo
The NosyRosie camera in action can be seen in the following video: https://www.dropbox.com/s/mll1k11jh7dabgi/NosyRosieDemo.mp4?dl=0. From the video, you can see the camera doing its regular pan operation until it detects a human in its field of view. 
In the subsequent pan cycle, notice that the camera focuses more on the zone where the human was detected. In essence, it's being "nosy." 

### Code
All the code for this project can be found in the folder containing this README. It is organized as follows:

nosy_rosie.py (main) -->

o	NosyVideoStream (camera_capture.py) - responsible for capturing frames from the camera

o 	PanTiltManager (pan_tilt_manager.py) - responsible for panning (and initially tilting) the camera

o	MotionHandler (motion_handler.py) - thread class that accesses and controls the PanTiltManager

o 	DisplayHandler (display_handler.py) - thread class responsible for displaying the frames on screen

o 	ImageHandler (image_handler.py) - thread class responsible for capturing frames for processing

o 	ImageProcessor (image_processor.py) - thread class responsible for performing object detection on a frame

o 	ZoneMap (zone_map.py) - class representing the degree zone map used by the motion handler for guiding the camera's focus

o 	ZoneMapHandler (zone_map_handler.py) - thread class to access and control the zone map


To execute: ./nosy_rosie.py from the command line


Dependencies: Requires OpenCV 3.4.3, Numpy
