#!/usr/bin/env python
import time
import numpy as np
import pantilthat as pt
from threading import Lock
		
#=======================================================================
# @brief Class that manages the motion of the Pimoroni pan-tilt-hat
#=======================================================================
class PanTiltManager:
	movementLock = Lock()
	PAN_ZERO = np.int(-5)
	TILT_ZERO = np.int(-30)
	
	def __init__(self, center = False):
		if center is True:
			self.reset()
		
	def panSlowly(self, degree):
		currDeg = pt.get_pan()
		
		if degree < currDeg:
			diff = currDeg - degree
			numHalfDegrees = diff / 0.5
			
			while currDeg >= degree:
				currDeg -= 0.5
				self.movementLock.acquire()
				pt.pan(currDeg)
				self.movementLock.release()
				time.sleep(0.005)
				
		else:
			diff = degree - currDeg	
			numHalfDegrees = diff / 0.5	
			
			while currDeg < degree:
				currDeg += 0.5
				self.movementLock.acquire()
				pt.pan(currDeg)
				self.movementLock.release()
				time.sleep(0.035)
				
		
	def panTo(self, degree, speed=20.0):
		self.movementLock.acquire()
		pt.pan(degree)
		self.movementLock.release()
		time.sleep(1.0 / speed)	
		
		
	def panToMillis(self, degree, millis):
		self.movementLock.acquire()
		pt.pan(degree)
		self.movementLock.release()
		time.sleep(millis / 1000.0)		
		
		
	def reset(self):
		pt.tilt(self.TILT_ZERO)
		self.panSlowly(self.PAN_ZERO)

