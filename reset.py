#!/usr/bin/env python
import math
import time
import sys
from pan_tilt_manager import PanTiltManager

try:
	ptMan = PanTiltManager()
	ptMan.reset()
	sys.exit(0)
				
except KeyboardInterrupt as k:
	print
	print ">>> Warning: Closing due to keyboard interrupt"
	print
		
except Exception as ex:
	print
	print ">>> Error: An unexpected exception has occurred"
	print ex
	print
		
