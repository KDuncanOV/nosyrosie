#!/usr/bin/env python


#=======================================================================
# @brief Class representing an image with timestamp and degree info
#=======================================================================
class NosyImage:
	image = []
	timestamp = 0
	degree = 0
	
	def __init__(self, img, ts, deg):
		self.image = img
		self.timestamp = ts
		self.degree = deg

	def getImage(self):
		return self.image
		
	def getTimestamp(self):
		return self.timestamp
		
	def getDegree(self):
		return self.degree
