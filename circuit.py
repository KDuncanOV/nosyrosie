#!/usr/bin/env python
import math
import time
import sys
from pan_tilt_manager import PanTiltManager


try:
	radians = 0
	cycles = 0
	ptMan = PanTiltManager()
	
	while True and cycles < 50:
		a = math.sin(radians) * 60

		if round(a, 3) >= 60.0:
			print "A:", round(a, 2)
			print "Cycles:", cycles
			cycles += 1
		
		a = int(a)
		ptMan.panTo(a, 50)		
		radians += 0.00872665
		
	sys.exit(0)
				
except KeyboardInterrupt as k:
	print
	print ">>> Warning: Closing due to keyboard interrupt"
	print
		
except Exception as ex:
	print
	print ">>> Error: An unexpected exception has occurred"
	print ex
	print
		
